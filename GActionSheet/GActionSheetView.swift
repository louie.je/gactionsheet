//
//  GActionSheetView.swift
//  GActionSheet
//
//  Created by Louie Je Decatoria on 27/03/2019.
//  Copyright © 2019 Louie Je Decatoria. All rights reserved.
//

import UIKit

public class GActionSheetView: UIView {
  
  @IBOutlet weak var holdView: UIView!
  
  let nibName = "GActionSheetView"
  var contentView: UIView!
  var dimView = UIView()
  var safeAreaBottomHeight: CGFloat = 0.0
  
  var timer: Timer?
  var titleFont = UIFont()
  
  var initialViewHeight: CGFloat = 30.0
  
  // MARK: Set Up View
  public override init(frame: CGRect) {
    // For use in code
    super.init(frame: frame)
    setUpView()
  }
  
  public required init?(coder aDecoder: NSCoder) {
    // For use in Interface Builder
    super.init(coder: aDecoder)
    if #available(iOS 11.0, *) {
      safeAreaBottomHeight = UIWindow().safeAreaInsets.bottom
    }
    setUpView()
    contentView.alpha = 0.0
  }
  
  
  private func setUpView() {
    let bundle = Bundle(for: type(of: self))
    let nib = UINib(nibName: self.nibName, bundle: bundle)
    self.contentView = nib.instantiate(withOwner: self, options: nil).first as? UIView
    addSubview(contentView)
    
    let width = self.frame.width
    let originY = self.frame.height
    contentView.frame.size.width = width
    contentView.frame.origin.y = originY
    contentView.frame.size.height += safeAreaBottomHeight
    
    holdView.layer.cornerRadius = 3
    
    contentView.autoresizingMask = []
    contentView.translatesAutoresizingMaskIntoConstraints = true
    
    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(removeSelf))
    self.addGestureRecognizer(tapGesture)
    
    let gesture = UIPanGestureRecognizer.init(target: self, action: #selector(GActionSheetView.panGesture))
    contentView.isUserInteractionEnabled = true
    contentView.addGestureRecognizer(gesture)
  }
}
// Provide functions to update view
extension GActionSheetView {
  public func add(item title: String, icon: String, order: Int, font: UIFont? = nil, action: @escaping () -> Void = {}) {
    let bHeight = self.frame.maxX / 8
    let bWidth = self.frame.maxX
    let item = GActionSheetItem(frame: CGRect(x: bHeight * 0.15, y: bHeight * CGFloat(order), width: bWidth, height: bHeight))
    let size = CGSize(width: bWidth, height: bHeight)
    item.addSubview(setIcon(icon: icon, size: size))
    item.addSubview(setTitle(title: title, size: size, font: font))
    item.whenButtonIsClicked(action: action)
    self.contentView.addSubview(item)
    self.contentView.frame.size.height += item.frame.height
  }
  private func setTitle(title: String, size: CGSize, font: UIFont? = nil) -> UILabel {
    let titleLabel = UILabel(frame: CGRect(x: size.height, y: 0, width: (size.width-size.height) * 0.95 , height: size.height))
    titleLabel.text = title
    titleLabel.numberOfLines = 0
    titleLabel.font = self.titleFont
    if let useFont = font {
      titleLabel.font = useFont
    }
    return titleLabel
  }
  private func setIcon(icon: String, size: CGSize) -> UIImageView {
    let iconImageView = UIImageView(frame: CGRect(x: size.height * 0.15, y: size.height * 0.15, width: size.height * 0.7, height: size.height * 0.7))
    iconImageView.contentMode = .scaleAspectFit
    iconImageView.image = UIImage(named: icon)
    
    return iconImageView
  }
  public func setTitleFont(font: UIFont) {
    self.titleFont = font
  }
}
// MARK: Set up Timer to show and Animation
extension GActionSheetView {
  public override func didMoveToSuperview() {
    addDimView()
    // Fade in when added to superview
    // Then add a timer to remove the view
    self.contentView.frame.origin.y = self.frame.maxY
    self.contentView.frame.size.height += initialViewHeight
    UIView.animate(withDuration: 0.15, animations: {
      self.contentView.alpha = 1.0
      self.contentView.frame.origin.y = self.frame.maxY - self.contentView.frame.height
    })
  }
  @objc private func removeSelf() {
    // Animate removal of view
    UIView.animate(
      withDuration: 0.15,
      animations: {
        self.contentView.frame.origin.y = self.frame.maxY
        self.contentView.alpha = 0.0
        self.dimView.alpha = 0.0
    }) { _ in
      self.removeFromSuperview()
    }
  }
  @IBAction func closeAction(_ sender: Any) {
    self.removeSelf()
  }
  private func addDimView() {
    self.dimView = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height))
    dimView.backgroundColor = UIColor.black
    dimView.alpha = 0.5
    self.insertSubview(dimView, at: 0)
  }
}
// Pan Gesture
extension GActionSheetView {
  @objc func panGesture(_ recognizer: UIPanGestureRecognizer) {
    let fullView = self.frame.maxY - self.contentView.frame.height
    let closeView: CGFloat = self.frame.maxY
    let partialView: CGFloat = closeView - fullView
    let percentToClose: CGFloat = 0.60
    let translation = recognizer.translation(in: self.contentView)
    let velocity = recognizer.velocity(in: self.contentView)
    let y = self.contentView.frame.minY
    
    if ( y + translation.y >= fullView) && (y + translation.y <= closeView ) {
      self.contentView.frame.origin.y = y + translation.y
      recognizer.setTranslation(CGPoint.zero, in: self)
    }

    if recognizer.state == .ended {
      var duration =  velocity.y < 0 ? Double((y - fullView) / -velocity.y) : Double((closeView - y) / velocity.y )
      let lastY = (closeView - self.contentView.frame.origin.y) / partialView
      duration = duration > 0.2 ? 0.2 : duration

      UIView.animate(withDuration: duration, delay: 0.0, options: [.allowUserInteraction], animations: {
        if lastY <= percentToClose {
          self.contentView.frame = CGRect(x: 0, y: closeView, width: self.contentView.frame.width, height: self.contentView.frame.height)
        } else {
          self.contentView.frame = CGRect(x: 0, y: fullView, width: self.contentView.frame.width, height: self.contentView.frame.height)
        }

      }, completion: { (finished: Bool) in
        if lastY <= percentToClose {
          self.removeSelf()
        }
      })
    }
  }
}

public class GActionSheetItem: UIButton {
  var buttonTitle: String?
  var buttonId: String?
  
  var action: (() -> Void)?
  
  func whenButtonIsClicked(action: @escaping () -> Void) {
    self.action = action
    self.addTarget(self, action: #selector(GActionSheetItem.clicked), for: .touchUpInside)
  }
  
  @objc func clicked() {
    action?()
  }
}
