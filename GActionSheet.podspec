Pod::Spec.new do |s|



  s.name         = "GActionSheet"
  s.version      = "1.0.0"
  s.summary      = "Custom action sheet that able to add icon, add items, and change font."

 
  s.description  = "Custom action sheet that able to add icon, add items, and change font."

  s.homepage     = "http://EXAMPLE/GcashRSA"
 

  s.license      = "MIT"
 

  s.author             = { "Louie Je Decatoria" => "louie.decatoria@mynt.xyz" }
 
  s.platform     = :ios, "9.0"


  s.source       = { :git => "git@gitlab.com:louie.je/gactionsheet.git", :tag => "#{s.version}" }
  s.resources = "GActionSheet/*.xib"
  s.resource_bundles = {
    'GActionSheet' => [
    'Pod/**/*.xib']
  }

  s.source_files  = "GActionSheet", "GActionSheet/*.{h,m,swift}"
  s.frameworks = "Foundation"

end
